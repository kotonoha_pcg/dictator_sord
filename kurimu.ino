//計算機工学実習 補講
//split method version

//define
int LED1 = 9;
int LED2 = 10;
int splitnum = 5;

void setup() {
    //watch var
    Serial.begin(9600);
}

void loop() {
    int ad0 = analogRead(A0);

    ad0 /= 4;
    //log
    Serial.pritln(ad0);

    writeptn1(ad0);
    writeptn2(ad0);

}

void writeptn1(int vVar) {
    analogWrite(LED1, vVar);
    analogWrite(LED2, 255);
    delay(1000);
}

void writeptn2(int vVar) {
    analogWrite(LED1, 255);
    analogWrite(LED2, vVar);
    delay(1000);
}